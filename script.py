__author__ = 'Moawiya'


file = "A-large-practice.in"


class Case:
    def __init__(self, smax, digits, num):
        self.smax = smax
        self.digits = digits
        self.num = num


def calculate_friends(test_case):
    needed = 0
    current = 0
    i = 0
    digits = list(test_case.digits)
    for digit in digits:
        if int(digit):
            if i <= current:
                current += int(digit)
            else:
                needed += (i - current)
                current += (i - current) + int(digit)
        i += 1
    test_case.needed = needed


def run_tests(cases, out):
    for case in cases:
        calculate_friends(case)
        out.write("Case #" + str(case.num) + ": " + str(case.needed) + '\n')


def main():
    f = open(file, 'r')
    content = f.readlines()
    f.close()

    i = 1
    cases = []
    for line in content[1:]:
        smax = (line.split(' '))[0]
        digits = (line.split(' '))[1]
        cases.append(Case(smax, digits.strip(), i))
        i += 1
    out = open("A-small.out", "w")
    run_tests(cases, out)
    out.close()


if __name__ == "__main__":
    main()
